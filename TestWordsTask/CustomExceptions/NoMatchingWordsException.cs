﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestWordsTask.CustomExceptions {
  public class NoMatchingWordsException: Exception {
    public NoMatchingWordsException(): base("There are no matching words") {

    }

    public NoMatchingWordsException(string message) : base(message) {
    }

    public NoMatchingWordsException(string message, Exception innerException) : base(message, innerException) {
    }
  }
}
