﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestWordsTask.Utility {
  public static class DictionaryPrintExtension {
    public static void PrintAsWordCombinations(this Dictionary<string, string> keyValues) {
      if (keyValues == null)
        throw new ArgumentNullException();
      foreach (var row in keyValues)
        Console.WriteLine(row.Key + " = " + row.Value);
    }
  }
}
