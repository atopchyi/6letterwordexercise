﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestWordsTask.CustomExceptions;

namespace TestWordsTask {
  public static class CombinationsFinder {
    public static Dictionary<string, string> FindWordsCombinations(string [] words, int searchWordLength) {
      var results = new Dictionary<string, string>();
      var wordsToSplit = words.Where(x => x.Length == searchWordLength).Distinct();
      if (wordsToSplit.Count() == 0)
        throw new NoMatchingWordsException();
      foreach (var word in wordsToSplit) {
        var wordCombinations = Permute(word).ToList();
        foreach (var combination in wordCombinations) {
          var combinationArray = combination.Split('+');
          if (combinationArray.Length > 1) {
            if (combinationArray.All(x => words.Contains(x)))
              results.Add(combination, word);
          }
        }
      }
      return results;
    }
    public static IEnumerable<string> Permute(string target) {
      if (target == null)
        throw new ArgumentNullException();
      if (target.Length <= 1) {
        yield return target;
        yield break;
      }
      var c = target[0];
      foreach (var rest in Permute(target.Remove(0, 1))) {
        yield return c + rest;
        yield return c + "+" + rest;
      }
    }
  }
}

