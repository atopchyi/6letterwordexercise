﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using TestWordsTask.CustomExceptions;
using TestWordsTask.DataReaders;
using TestWordsTask.Utility;

namespace TestWordsTask {
  class Program {
    private const int WORD_LENGTH = 6;
    static void Main(string[] args) {
      var builder = new ConfigurationBuilder()
      .SetBasePath(Directory.GetCurrentDirectory())
      .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
      var configuration = builder.Build();
      
      try {
        var localFilePath = configuration.GetSection("FileLocations").GetSection("LocalFile").Value;
        DataReader reader = new FileReader(localFilePath);
        var words = reader.GetData();
        var combinations = CombinationsFinder.FindWordsCombinations(words, WORD_LENGTH);
        combinations.PrintAsWordCombinations();
      }
      catch(FileNotFoundException ex) {
        Console.WriteLine(ex.Message);
      }
      catch(NoMatchingWordsException ex) {
        Console.WriteLine(ex.Message);
      }
      catch(ArgumentNullException ex) {
        Console.WriteLine(ex.Message);
      }
      catch {
        Console.WriteLine("Something went wrong...");
        throw;
      }
      Console.ReadKey();
    }
  }
}
