﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestWordsTask {
  public abstract class DataReader {
    public string FilePath { get; private set; }
    public DataReader(string filePath) => FilePath = filePath;
    public abstract string[] GetData();
  }
}
