﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TestWordsTask.DataReaders {
  public class FileReader : DataReader {
    public FileReader(string path) : base(path) { }
    public override string [] GetData() {
      if (!File.Exists(FilePath))
        throw new FileNotFoundException();
      var words = File.ReadAllLines(FilePath);
      return words;
    }
  }
}
