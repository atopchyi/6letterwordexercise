﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestWordsTask;

namespace TestWordsTest {
  [TestClass]
  public class CombinationsFinderTest {
    [TestMethod]
    public void PermuteTest() {
      var wordToTest = "abc";
      var expected = new List<string>() { "abc", "a+bc", "ab+c", "a+b+c" };
      CollectionAssert.AreEqual(expected, CombinationsFinder.Permute(wordToTest).ToList());
    }
    [TestMethod]
    public void FindWordsCombinationsTest() {
      var moqWords = new string[] { "abc", "ab", "a", "b", "c", "bc" };
      var moqWordLength = 3;
      var expected = new Dictionary<string, string>();
      expected.Add("a+bc", "abc");
      expected.Add("ab+c", "abc");
      expected.Add("a+b+c", "abc");
      CollectionAssert.AreEqual(expected.OrderBy(kv => kv.Key).ToList(), CombinationsFinder
        .FindWordsCombinations(moqWords, moqWordLength).OrderBy(kv => kv.Key).ToList());
    }

  }
}
